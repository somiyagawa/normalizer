Normalizes orthography and removes diacritics from UTF-8 Coptic.

Copyright 2013-14 Amir Zeldes, Caroline T. Schroeder.  The perl program is free software. You may copy or redistribute the script under the same terms as Perl itself.

Additional material copyright 2013-14 Amir Zeldes, Caroline T. Schroeder: this is free software distributed under the GNU General Public license v. 3. http://www.gnu.org/licenses/gpl.html. You are welcome to distribute it under the conditions outlined in the license.